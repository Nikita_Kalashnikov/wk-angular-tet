import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ROUTES_CONFIG} from "../app-config/routes/routes.config";
import {LoginComponent} from "./features/login/login.component";
import {ProfileComponent} from "./features/profile/profile.component";
import {AuthGuard} from "./core/services/guards/auth.guard";

const routes: Routes = [
  {
    path: ROUTES_CONFIG.LOGIN_PAGE,
    component: LoginComponent,
  },
  {
    path: `${ROUTES_CONFIG.PROFILE_PAGE}/:userId`,
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: ROUTES_CONFIG.LOGIN_PAGE,
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: ROUTES_CONFIG.LOGIN_PAGE,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
