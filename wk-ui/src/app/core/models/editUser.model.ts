export interface EditUserModel {
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
}
