export interface UserModel {
  id: number,
  firstName: string,
  lastName: string,
  email: string,
  phone: string,
  photoName: string | null,
  title: string,
  shortTitle: string
}
