import {Injectable} from '@angular/core';
import {UserModel} from "../models/user.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";

const httpOptions: object = {
  responseType: 'text'
};

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private currentUser: UserModel | null = null;
  private photoUploadUrl: string = 'http://localhost:5000/upload-photo';
  private photoUrl: string = 'http://localhost:5000/get-photo';
  private editUserUrl: string = 'http://localhost:5000/edit-user';
  private isFileUploaded: boolean = false;
  private userPhoto;

  constructor(private http: HttpClient) {
  }

  public editUser() {
    return this.http.post(`${this.editUserUrl}/${this.currentUser.id}`, this.currentUser, httpOptions)
  }

  public uploadPhoto(file: File, userId) {
    const formData = new FormData();
    formData.append('file', file);

    return this.http.post(`${this.photoUploadUrl}/${userId}`, formData, httpOptions);
  }

  public getUserPhoto(photoName: string) {
    return this.http.get(`${this.photoUrl}/${photoName}`, httpOptions);
  }

  public getUser(): UserModel {
    return this.currentUser
  }

  public setUser(user: UserModel | null) {
    this.currentUser = user;
  }

  public getPhotoState() {
    return this.isFileUploaded
  }

  public setPhotoState(bool) {
    this.isFileUploaded = bool;
  }

  public setUserPhoto(userPhoto) {
    this.userPhoto = userPhoto;
  }

  public getCurrentUserPhoto() {
    return this.userPhoto;
  }
}
