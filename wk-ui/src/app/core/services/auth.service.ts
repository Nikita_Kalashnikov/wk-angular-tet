import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HttpHeaders} from '@angular/common/http';
import {UserModel} from "../models/user.model";

const httpOptions: object = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated: boolean = false;
  private authUrl: string = 'http://localhost:5000/login';
  private getUserUrl: string = 'http://localhost:5000/users';

  constructor(private http: HttpClient) {
  }

  public login(email: string, password: string) {
    return this.http.post(this.authUrl, {
      email: email,
      password: password
    }, httpOptions)
  };

  public getCurrentUser(id: string) {
    return this.http.get<UserModel>(`${this.getUserUrl}/${id}`)
  }

  public getAuthenticatedState(): boolean {
    return this.isAuthenticated
  }

  public setAuthentication(isAuthenticated: boolean) {
    this.isAuthenticated = isAuthenticated;
  }
}
