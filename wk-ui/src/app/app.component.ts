import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'wkui-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'wk-ui';

  ngOnInit(): void {
  }
}
