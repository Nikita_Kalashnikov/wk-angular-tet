import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {RouterModule} from "@angular/router";

import {AppComponent} from './app.component';
import {LoginComponent} from './features/login/login.component';
import {ProfileComponent} from './features/profile/profile.component';
import {EmailFieldComponent} from './shared/components/email-field/email-field.component';
import {PasswordFieldComponent} from './shared/components/password-field/password-field.component';

import {AuthGuard} from "./core/services/guards/auth.guard";
import {AuthService} from "./core/services/auth.service";
import {ProfileHeaderComponent} from './shared/components/profile-header/profile-header.component';
import {BasicInfoComponent} from './shared/components/basic-info/basic-info.component';
import {MatMenuModule} from "@angular/material/menu";
import {MatTooltipModule} from "@angular/material/tooltip";
import {ngfModule} from "angular-file";
import {ProfileTabsComponent} from './shared/components/profile-tabs/profile-tabs.component';
import {MatTabsModule} from "@angular/material/tabs";
import {InputFieldComponent} from './shared/components/input-field/input-field.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    EmailFieldComponent,
    PasswordFieldComponent,
    ProfileHeaderComponent,
    BasicInfoComponent,
    ProfileTabsComponent,
    InputFieldComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
    MatMenuModule,
    MatTooltipModule,
    ngfModule,
    MatTabsModule
  ],
  providers: [AuthGuard, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
