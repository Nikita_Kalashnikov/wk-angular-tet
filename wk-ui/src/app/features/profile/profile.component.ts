import {Component, OnInit} from '@angular/core';
import {ProfileService} from "../../core/services/profile.service";
import {UserModel} from "../../core/models/user.model";

@Component({
  selector: 'wkui-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  private user: UserModel;

  constructor(private profileService: ProfileService) {
  }

  ngOnInit() {
    this.user = this.profileService.getUser();
  }
}
