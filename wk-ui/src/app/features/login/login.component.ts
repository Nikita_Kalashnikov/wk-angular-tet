import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../core/services/auth.service";
import {Router} from "@angular/router";
import {ROUTES_CONFIG} from "../../../app-config/routes/routes.config"
import {UserModel} from "../../core/models/user.model";
import {ProfileService} from "../../core/services/profile.service";

@Component({
  selector: 'wkui-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private email: string = '';
  private password: string = '';

  constructor(private authService: AuthService,
              private router: Router,
              private profileService: ProfileService) {
  }

  ngOnInit() {
    const userId = localStorage.getItem('userId');
    if (userId) {
      this.authService.getCurrentUser(userId)
        .subscribe(res => {
          this.authService.setAuthentication(true);
          this.router.navigate([ROUTES_CONFIG.PROFILE_PAGE, res.id]);
          this.profileService.setUser(res);
        })
    }
  }

  private tryLogin(email: string, password: string) {
    this.authService.login(email, password).subscribe((res: UserModel) => {
      this.authService.setAuthentication(true);
      this.router.navigate([ROUTES_CONFIG.PROFILE_PAGE, res.id]);
      this.profileService.setUser(res);
      localStorage.setItem('userId', `${res.id}`);
    })
  }
}
