import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'wkui-password-field',
  templateUrl: './password-field.component.html',
  styleUrls: ['./password-field.component.scss']
})
export class PasswordFieldComponent {
  @Input()
  public password: string = '';

  @Output()
  public passwordChange: EventEmitter<string> = new EventEmitter<string>();

  private hidePassword = true;
  private passwordFormControl: FormControl = new FormControl('', [
    Validators.required
  ]);

  public emitPasswordChange() {
    this.passwordChange.emit(this.password);
  }
}
