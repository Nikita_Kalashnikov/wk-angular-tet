import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'wkui-email-field',
  templateUrl: './email-field.component.html',
  styleUrls: ['./email-field.component.scss']
})
export class EmailFieldComponent {
  @Input()
  public email: string = '';
  @Input()
  public label: string = '';

  @Output()
  public emailChange: EventEmitter<string> = new EventEmitter<string>();

  private emailFormControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  public emitEmailChange() {
    this.emailChange.emit(this.email);
  }
}
