import {Component, OnInit} from '@angular/core';
import {ProfileService} from "../../../core/services/profile.service";
import {UserModel} from "../../../core/models/user.model";
import {AuthService} from "../../../core/services/auth.service";
import {Router} from "@angular/router";
import {ROUTES_CONFIG} from "../../../../app-config/routes/routes.config";

@Component({
  selector: 'wkui-profile-header',
  templateUrl: './profile-header.component.html',
  styleUrls: ['./profile-header.component.scss']
})
export class ProfileHeaderComponent implements OnInit{
  private userPhoto;
  private user: UserModel = this.profileService.getUser();

  constructor(private profileService: ProfileService,
              private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.profileService.getUserPhoto(this.user.photoName)
      .subscribe(res => {
        this.userPhoto = res;
      });
  }

  private logOut() {
    this.authService.setAuthentication(false);
    this.profileService.setUser(null);
    localStorage.removeItem('userId');
    this.router.navigate([ROUTES_CONFIG.LOGIN_PAGE]);
  }
}
