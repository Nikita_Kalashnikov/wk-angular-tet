import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'wkui-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent {

  @Input()
  public model: string = '';
  @Input()
  private label: string = '';

  @Output()
  public modelChange: EventEmitter<string> = new EventEmitter<string>();


  public emitModelChange() {
    this.modelChange.emit(this.model);
  }

}
