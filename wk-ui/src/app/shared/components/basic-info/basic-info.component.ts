import {Component, OnInit} from '@angular/core';
import {ProfileService} from "../../../core/services/profile.service";
import {UserModel} from "../../../core/models/user.model";

@Component({
  selector: 'wkui-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent implements OnInit{
  private userPhoto;
  private user: UserModel = this.profileService.getUser();

  constructor(private profileService: ProfileService) {
  }

  ngOnInit() {
    this.profileService.getUserPhoto(this.user.photoName)
      .subscribe(res => {
        this.userPhoto = res;
      });
  }

  uploadPhoto(file: File) {
    this.profileService.setPhotoState(true);
    this.profileService.setUserPhoto(this.userPhoto);
    this.profileService.uploadPhoto(file, this.user.id).subscribe();
  }
}
