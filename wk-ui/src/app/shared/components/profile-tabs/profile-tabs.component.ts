import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ProfileService} from "../../../core/services/profile.service";
import {UserModel} from "../../../core/models/user.model";
import {EditUserModel} from "../../../core/models/editUser.model";

@Component({
  selector: 'wkui-profile-tabs',
  templateUrl: './profile-tabs.component.html',
  styleUrls: ['./profile-tabs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileTabsComponent implements OnInit {
  private noInformationYet: string = 'No information yet.';
  private user: UserModel;
  private isEditingMode = false;
  private editedUser: EditUserModel;

  constructor(private profileService: ProfileService) {
  }

  public turnEditingMode() {
    this.isEditingMode = !this.isEditingMode;
  }

  public cancelEditing() {
    this.editedUser = {
      email: this.user.email,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      phone: this.user.phone
    };
    this.turnEditingMode();
  }

  public editUser() {
    this.user.email = this.editedUser.email;
    this.user.firstName = this.editedUser.firstName;
    this.user.lastName = this.editedUser.lastName;
    this.user.phone = this.editedUser.phone;
    this.profileService.setUser(this.user);
    this.profileService.editUser().subscribe(res => {
      this.turnEditingMode();
    });
  }

  ngOnInit() {
    this.user = this.profileService.getUser();
    this.editedUser = {
      email: this.user.email,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      phone: this.user.phone
    };
  }
}
