import express from 'express';
import bodyParser from 'body-parser';
import fs from 'fs';
import cors from 'cors';
import fileUpload from 'express-fileupload';
import {UserModel} from "../core/models/user.model";

const app = express();
const port: number = 5000;

app.use(cors());
app.use(fileUpload());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.post('/login', (req, res) => {
  fs.readFile('./storage/users.json', 'UTF-8', (err, data: string) => {
    if (err) throw err;

    const rawData = data;
    const users: UserModel[] = JSON.parse(rawData);
    const currentUser: UserModel = users.find(user => user.email === req.body.email);

    if (currentUser) {
      if (currentUser.password === req.body.password) {
        const {password, ...preparedUser} = currentUser;
        res.send(preparedUser);
      } else {
        res.sendStatus(400);
      }
    } else {
      res.sendStatus(404);
    }
  });
});

app.get('/users/:id', (req, res) => {
  fs.readFile('./storage/users.json', 'UTF-8', (err, data: string) => {
    if (err) throw err;

    const rawData = data;
    const users: UserModel[] = JSON.parse(rawData);
    const currentUser: UserModel = users.find(user => user.id === Number(req.params.id));

    if (currentUser) {
      const {password, ...finalUser} = currentUser;
      res.send(finalUser);
    } else {
      res.sendStatus(404);
    }
  });
});

app.post('/upload-photo/:id', (req, res) => {
  fs.readFile('./storage/users.json', 'UTF-8', (err, data: string) => {
    if (err) throw err;

    const rawData = data;
    const userPhoto = req.files.file;
    const users: UserModel[] = JSON.parse(rawData);
    users.forEach((user) => {
      if (user.id === Number(req.params.id)) {
        user.photoName = userPhoto.name;
      }
    });
    const jsonData = JSON.stringify(users);
    fs.writeFileSync('./storage/users.json', jsonData);

    userPhoto.mv(`./storage/userPhotos/${req.files.file.name}`, (err) => {
      if (err) return res.sendStatus(500).send(err);

      res.sendStatus(200);
    });
  });
});

app.get('/get-photo/:photo', (req, res) => {
  fs.readFile(`./storage/userPhotos/${req.params.photo}`, (err, data) => {
    if (data) {
      const base64data: string = `data:image/png;base64,${data.toString('base64')}`;

      res.send(base64data);
    } else {
      fs.readFile('./storage/userPhotos/photo-default.jpg', (err, data) => {
        const base64data: string = `data:image/png;base64,${data.toString('base64')}`;

        res.send(base64data);
      })
    }
  })
});


app.post('/edit-user/:userId', (req, res) => {
  fs.readFile('./storage/users.json', 'UTF-8', (err, data: string) => {
    if (err) throw err;
    const rawData = data;
    const users: UserModel[] = JSON.parse(rawData);

    users.forEach(user => {
      if (user.id === Number(req.params.userId)) {
        user.firstName = req.body.firstName;
        user.email = req.body.email;
        user.lastName = req.body.lastName;
        user.phone = req.body.phone;
      }
    });

    const jsonData = JSON.stringify(users);
    fs.writeFileSync('./storage/users.json', jsonData);

    res.sendStatus(200);
  });
});

app.listen(port, () => {
  console.log(`Application has been started on ${port}`);
});